#!/bin/bash
url_api=$(gp url 9090)
processName="hello-sat-02"

data="{\"processName\":\"${processName}\"}"

curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/kill"

