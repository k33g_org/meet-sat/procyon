#!/bin/bash
url_api=$(gp url 9090)
satRegistry="https://gitlab.com/api/v4/projects/33178966/packages/generic"
wasmFile="hello.wasm"
version="0.0.0"
packageName="hello-sat"
processName="hello-sat-02"
httpPort="8082"

data="{\"satRegistry\":\"${satRegistry}\",\"wasmFile\":\"${wasmFile}\",\"version\":\"${version}\",\"packageName\":\"${packageName}\",\"processName\":\"${processName}\",\"httpPort\":${httpPort}}"

#echo "${data}" > data.json

curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/launch"

