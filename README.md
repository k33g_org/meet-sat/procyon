# Procyon
> a small **[sat](https://github.com/suborbital/sat)** launcher

> Suborbital CLI and Sat are installed at the startup

## Pre-requisites

- Node.js
- Sat: [Meet Sat [Part 3]: Run Sat on a Pi4](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/100)

## Install

```bash
git clone https://gitlab.com/k33g_org/meet-sat/procyon.git
cd procyon
npm install
```

## Start Procyon

```bash
screen
cd procyon
npm start
ctrl a + d
```
> Procyon is listening on 9090

## Use Procyon

### Start a Sat process

```bash
#!/bin/bash
url_api="http://192.168.1.42:9090"
satRegistry="https://gitlab.com/api/v4/projects/33178966/packages/generic"
wasmFile="hello.wasm"
version="0.0.0"
packageName="hello-sat"
processName="hello-sat-01"
httpPort="8081"

data="{\"satRegistry\":\"${satRegistry}\",\"wasmFile\":\"${wasmFile}\",\"version\":\"${version}\",\"packageName\":\"${packageName}\",\"processName\":\"${processName}\",\"httpPort\":${httpPort}}"

#echo "${data}" > data.json

curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/launch"
```

### Call the service

```bash
#!/bin/bash
url_api="http://192.168.1.42:8081"
curl ${url_api} -d 'Bob Morane'
echo ""
```

### Get list of Sat processes

```bash
#!/bin/bash
url_api="http://192.168.1.42:9090"
curl ${url_api}/list
```

### Kill a Sat process by name

> 🚧 wip 

```bash
#!/bin/bash
url_api="http://192.168.1.42:9090"
processName="hello-sat-01"

data="{\"processName\":\"${processName}\"}"

curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/kill"
```
