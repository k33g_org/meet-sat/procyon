const spawn = require('child_process').spawn

/*
SAT_HTTP_PORT=8080 bash -c "exec -a hello-sat sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}"
*/
function launch(jsonParameters, request, reply, options, fastify) {
  // {wasmFile, version, packageName, processName, satRegistry, httpPort}

  let wasmFile = jsonParameters.wasmFile
  let version = jsonParameters.version
  let packageName = jsonParameters.packageName
  let processName = jsonParameters.processName
  let httpPort = jsonParameters.httpPort
  let satRegistry = jsonParameters.satRegistry

  console.log("😛", jsonParameters)

  let wasmUrl = `${satRegistry}/${packageName}/${version}/${wasmFile}`

  console.log("🌍", wasmUrl)
  // SAT_HTTP_PORT=8080 sat ${satRegistry}/${packageName}/${version}/${wasmFile}

  let child = spawn(
    "bash",
    [
      "-c",
      `SAT_HTTP_PORT=${httpPort} exec -a ${processName} sat ${wasmUrl}`
    ],
    process.env
  )

  child.on('exit', _ => {
    fastify.log.info('🤖 sat exited!')
  })

  child.stdout.once('data', (data) => {
    fastify.log.info(`🤖 ${data}`)
    reply.header('Content-Type', 'application/text; charset=utf-8')
      .send(data) 
  })

  child.stderr.once('data', (data) => {
    fastify.log.error(`😡 ${data}`)
    reply
      .header('Content-Type', 'application/text; charset=utf-8')
      .code(500)
      .send(data)
  })

}

function kill(jsonParameters, request, reply, options, fastify) {
  // {processName}
  let processName = jsonParameters.processName

  let child = spawn("pkill", ["-f", processName], process.env)

  child.on('exit', _ => {
    fastify.log.info('🤖 sat exited!') // change the message
  })

  child.stdout.once('data', (data) => {
    fastify.log.info(`🤖 ${data}`)
    reply.header('Content-Type', 'application/text; charset=utf-8')
      .send(data) 
  })

  child.stderr.once('data', (data) => {
    fastify.log.error(`😡 ${data}`)
    reply
      .header('Content-Type', 'application/text; charset=utf-8')
      .code(500)
      .send(data)
  })

}

function list(request, reply, options, fastify) {

  let child = spawn("ps", ["-fC", "sat"], process.env)

  child.on('exit', _ => {
    fastify.log.info('🤖 sat exited!') // change the message
  })

  child.stdout.once('data', (data) => {
    fastify.log.info(`🤖 ${data}`)
    reply.header('Content-Type', 'application/text; charset=utf-8')
      .send(data) 
  })

  child.stderr.once('data', (data) => {
    fastify.log.error(`😡 ${data}`)
    reply
      .header('Content-Type', 'application/text; charset=utf-8')
      .code(500)
      .send(data)
  })

}

async function satProcesses (fastify, options) {
/*
```bash
url_api=$(gp url 9090)
satRegistry="https://gitlab.com/api/v4/projects/33178966/packages/generic"
wasmFile="hello.wasm"
version="0.0.0"
packageName="hello-sat"
processName="hello-sat-01"
httpPort="8080"

data="{\"satRegistry\":\"${satRegistry}\",\"wasmFile\":\"${wasmFile}\",\"version\":\"${version}\",\"packageName\":\"${packageName}\",\"processName\":\"${processName}\",\"httpPort\":${httpPort}}"

curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/launch"
``` 

```bash
#!/bin/bash
# Change the GitLab project ID
project_id="33178966"
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"

SAT_HTTP_PORT=8080 sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}

SAT_HTTP_PORT=8080 bash -c "exec -a hello-sat sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}"

```
*/
  fastify.post(`/launch`, async (request, reply) => {
    let jsonParameters = request.body
    
    let wasmFile = jsonParameters.wasmFile
    let version = jsonParameters.version
    let packageName = jsonParameters.packageName
    let processName = jsonParameters.processName
    let httpPort = jsonParameters.httpPort
    let satRegistry = jsonParameters.satRegistry

    console.log("👋", jsonParameters)

    launch({wasmFile, version, packageName, processName, satRegistry, httpPort}, request, reply, options, fastify)
    
    await reply
  })

  fastify.post(`/kill`, async (request, reply) => {
    let jsonParameters = request.body

    let processName = jsonParameters.processName
    
    kill({processName}, request, reply, options, fastify)
    
    await reply
  })

  fastify.get(`/list`, async (request, reply) => {
    
    list(request, reply, options, fastify)
    
    await reply
  })

}

module.exports = satProcesses
