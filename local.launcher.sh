#!/bin/bash
url_api=$(gp url 9090)
satRegistry="https://gitlab.com/api/v4/projects/33178966/packages/generic"
wasmFile="hello.wasm"
version="0.0.0"
packageName="hello-sat"
processName="hello-sat-01"
httpPort="8080"

#SAT_HTTP_PORT=8080 sat ${satRegistry}/${packageName}/${version}/${wasmFile}

bash -c "SAT_HTTP_PORT=8081 exec -a ${processName} sat ${satRegistry}/${packageName}/${version}/${wasmFile}"

pkill -f hello-sat-2